#include <iostream>
#include <filesystem>
#include <fstream>
#include <zipper/zipper.h>

int main(int argc, char** argv) {
	
    if(argc != 3) {
        std::cerr << "Usage: " << argv[0] <<" inputDirectory outputFilePath \n";
    }
	
	std::string inputPath = argv[1];
	std::string outputFilePath = argv[2];
	
	zipper::Zipper zipper(outputFilePath);
	
	//std::vector<std::string> files;
	const std::filesystem::path PATH{inputPath};
	if(std::filesystem::is_directory(PATH)){
		for(auto const& dir_entry: std::filesystem::recursive_directory_iterator{PATH}){
			if (std::filesystem::is_regular_file(dir_entry)){
				// std::cout << std::filesystem::canonical(dir_entry) << '\n';
				// files.push_back(std::filesystem::canonical(dir_entry));
				
				zipper.add(std::filesystem::canonical(dir_entry));
				
			}
		}
	}
	zipper.close();
	return 0;
	
	
}
