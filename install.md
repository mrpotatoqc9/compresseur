download and build zipper wrapper
```
sudo apt-get install zlib1g-dev
git clone --recursive https://github.com/sebastiandev/zipper.git
cd zipper
mkdir build
cd build
cmake ../
make
sudo make install
ldconfig
```
or checkout the official documentation:
https://github.com/sebastiandev/zipper
