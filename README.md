# crompressing folder with c++ zipper wrapper


### install dependencies
- see install.md file


### run
```
mkdir build && cd build
cmake ..
make

./zipzap ../test ziptest.zip
```

[comment]: <> (g++ -W -Wall -std=c++17 main.cpp -o zipzap `pkg-config zipper --cflags --libs`)
